/*
 * Copyright (c) 2021-2022 Francisco Miguel P.G. - Noisivelet
 * Unless stated otherwise, modification, distribution or comertialitation of this software is prohibited by law.
 */
package net.noisivelet.mlphelper.listeners;

import java.awt.Color;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.channel.ChannelType;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.concrete.ForumChannel;
import net.dv8tion.jda.api.entities.channel.forums.ForumPost;
import net.dv8tion.jda.api.entities.channel.forums.ForumTag;
import net.dv8tion.jda.api.entities.channel.forums.ForumTagSnowflake;
import net.dv8tion.jda.api.entities.channel.unions.GuildChannelUnion;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.command.UserContextInteractionEvent;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.selections.SelectMenu;
import net.dv8tion.jda.api.utils.messages.MessageCreateBuilder;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import net.dv8tion.jda.api.utils.messages.MessageEditData;
import net.noisivelet.discordbot.DiscordBot;
import net.noisivelet.mlphelper.utils.EmbededMessages;
import net.noisivelet.mlphelper.utils.Utils;

/**
 *
 * @author Francis
 */
public class CommandListener extends ListenerAdapter{
    
    @Override
    public void onUserContextInteraction(UserContextInteractionEvent event){
        String nombre=event.getName();
        switch(nombre){
            /*case "Ver Strikes" -> {
                event.deferReply(true).submit();
                DiscordBot.EXECUTOR.submit(() -> {
                    event.getHook().editOriginal(MessageEditData.fromCreateData(Utils.verStrikesMessage(event.getTarget(), event.getGuild()))).submit();
                });
                return;
            }
            case "Registro de listas" -> {
                event.reply(Utils.verRegistroListasMessage(event.getTarget())).setEphemeral(true).submit();
            }*/
        }
    }
    
    @Override
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        DiscordBot.EXECUTOR.submit(()-> {
            try{
                
                Member m=event.getMember();
                switch(event.getName()){
                    case "listaetiquetas" -> {
                        GuildChannelUnion canal=event.getOption("canal").getAsChannel();
                        if(canal.getType() != ChannelType.FORUM){
                            event.reply(EmbededMessages.errorEmbed("Tipo de canal incorrecto", "El canal especificado no es un foro.")).setEphemeral(true).queue();
                            return;
                        }
                        
                        ForumChannel foro=canal.asForumChannel();
                        List<ForumTag> tags=foro.getAvailableTags();
                        String msg="";
                        for(ForumTag tag : tags){
                            msg+="**"+tag.getName()+"**: `"+tag.getId()+"`\n";
                        }
                        
                        event.reply(EmbededMessages.successEmbed("Lista de IDs de las etiquetas del foro", "*Foro: "+foro.getAsMention()+"*\n"+msg)).setEphemeral(true).queue();
                    }

                    default -> {
                        event.reply(EmbededMessages.errorEmbed("Comando desconocido", "Ha ocurrido un error ejecutando el comando - Este error es del creador del bot y debe resolverlo él.\nCódigo de error `404-cmd_l`.")).setEphemeral(true).submit();
                        return;
                    }

                }
            } catch(Exception ex){
                ex.printStackTrace();
            }
        });
    }
}

