/*
 * Copyright (c) 2021-2022 Francisco Miguel P.G. - Noisivelet
 * Unless stated otherwise, modification, distribution or comertialitation of this software is prohibited by law.
 */
package net.noisivelet.mlphelper.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.Color;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.utils.messages.MessageCreateBuilder;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;

/**
 *
 * @author Francis
 */
public class JSONUnpacker {
    public static MessageCreateData buildEmbed(String json) throws JsonProcessingException, IllegalStateException, NullPointerException{
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node=mapper.readTree(json);
        MessageCreateBuilder msgbuilder=new MessageCreateBuilder();
        if(node.has("embed")) {
            JsonNode embed_node=node.get("embed");
            EmbedBuilder builder=new EmbedBuilder();
            if(embed_node.has("url"))
                builder.setTitle(embed_node.get("title").textValue(), embed_node.get("url").textValue());
            else
                builder.setTitle(embed_node.get("title").textValue());
            builder.setColor(new Color(embed_node.get("color").asInt()));
            if(embed_node.has("timestamp"))
                builder.setTimestamp(DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(embed_node.get("timestamp").textValue()));
            if(embed_node.has("description"))
                builder.setDescription(embed_node.get("description").textValue());

            if(embed_node.has("footer")){
                JsonNode footer=embed_node.get("footer");
                if(footer.has("icon_url"))
                    builder.setFooter(footer.get("text").textValue(), footer.get("icon_url").textValue());
                else
                    builder.setFooter(footer.get("text").textValue());
            }

            if(embed_node.has("thumbnail"))
                builder.setThumbnail(embed_node.get("thumbnail").get("url").textValue());

            if(embed_node.has("image"))
                builder.setImage(embed_node.get("image").get("url").textValue());

            if(embed_node.has("author")){
                JsonNode author=embed_node.get("author");
                String url=author.get("url") == null? null : author.get("url").textValue();
                String icon_url=author.get("icon_url") == null? null : author.get("icon_url").textValue();
                builder.setAuthor(author.get("name").textValue(), url, icon_url);
            }

            if(embed_node.has("fields")){
                JsonNode fields=embed_node.get("fields");
                Iterator<JsonNode> it=fields.elements();
                while(it.hasNext()){
                    JsonNode field_actual=it.next();
                    boolean inline;
                    if(field_actual.has("inline"))
                        inline=field_actual.get("inline").asBoolean();
                    else
                        inline=false;
                    builder.addField(field_actual.get("name").textValue(), field_actual.get("value").textValue(), inline);
                }
            }
            msgbuilder.setEmbeds(builder.build());
        }
        
        
        if(node.has("content"))
            msgbuilder.setContent(node.get("content").textValue());
        return msgbuilder.build();
    }
}
