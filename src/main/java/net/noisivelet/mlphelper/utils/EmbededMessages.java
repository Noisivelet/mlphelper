/*
 * Copyright (c) 2021-2022 Francisco Miguel P.G. - Noisivelet
 * Unless stated otherwise, modification, distribution or comertialitation of this software is prohibited by law.
 */
package net.noisivelet.mlphelper.utils;

import java.awt.Color;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.utils.messages.MessageCreateBuilder;
import net.dv8tion.jda.api.utils.messages.MessageCreateData;
import net.noisivelet.discordbot.DiscordBot;

/**
 *
 * @author Francis
 */
public final class EmbededMessages {
    public static MessageCreateData errorEmbed(String title, String description){
        return new 
        MessageCreateBuilder().setEmbeds(new EmbedBuilder()
                .setTitle("Error: "+title)
                .setDescription(description)
                .setColor(Color.RED)
                .build()
        ).build();
    }
    
    public static MessageCreateData successEmbed(String title, String description){
        return new 
        MessageCreateBuilder().setEmbeds(new EmbedBuilder()
                .setTitle(title)
                .setDescription(description)
                .setColor(Color.GREEN)
                .build()
        ).build();
    }
    
    public static MessageCreateData normalEmbed(String title, String description, Color color){
        return new
            MessageCreateBuilder().setEmbeds(new EmbedBuilder()
                    .setTitle(title)
                    .setDescription(description)
                    .setColor(color)
                    .build()
            ).build();
    }
}

