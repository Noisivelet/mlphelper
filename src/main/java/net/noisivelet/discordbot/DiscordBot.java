/*
 * Copyright (c) 2021-2022 Francisco Miguel P.G. - Noisivelet
 * Unless stated otherwise, modification, distribution or comertialitation of this software is prohibited by law.
 */
package net.noisivelet.discordbot;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.interactions.commands.Command;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.requests.restaction.CommandListUpdateAction;
import net.noisivelet.mlphelper.utils.Database;
import net.noisivelet.mlphelper.listeners.CommandListener;

/**
 *
 * @author Francis
 */
public class DiscordBot {
    static{
        System.setProperty("java.util.logging.SimpleFormatter.format",
              "[%1$tF %1$tT] [%4$-7s] %5$s %n");
    }
    public static void debug(String str){logger.log(Level.FINE, str);}
    public static void debug2(String str){logger.log(Level.FINER, str);}
    public static void log(String str){logger.log(Level.INFO, str);}
    public static void logln(){logger.log(Level.INFO, "\n");}
    public static void error(String str){logger.log(Level.SEVERE, str);}
    public static void warning(String str){logger.log(Level.WARNING, str);}
    public static final Logger logger=Logger.getLogger("MLPHelper");
    public static JDA jda;
    public static final ExecutorService EXECUTOR=Executors.newCachedThreadPool();
    public static final ScheduledExecutorService SCHEDULER=Executors.newScheduledThreadPool(8);
    
    public static void main(String... args){
        try{
            DiscordBot.log("Iniciando...");
            long inicio=System.currentTimeMillis();
            jda=buildJDA();
            DiscordBot.log("BuildJDA finalizado: "+(System.currentTimeMillis()-inicio)+"ms.");
            if(jda == null){
                DiscordBot.log("Error inicializando el bot. No se puede continuar.");
                return;
            }
            try {
                DiscordBot.log("Inicializando la base de datos...");
                long inicio_db=System.currentTimeMillis();
                Database.start();
                DiscordBot.log("Base de datos inicializada: "+(System.currentTimeMillis()-inicio_db)+"ms.");
            } catch (SQLException ex) {
                System.err.println("No se puede obtener acceso a la base de datos. Error: ");
                ex.printStackTrace();
                jda.shutdownNow();
                return;
            }

            long inicio_listeners=System.currentTimeMillis();
            DiscordBot.log("Añadiendo listeners...");
            addListeners();
            DiscordBot.log("Añadiendo comandos...");
            registerCommands();
            DiscordBot.log("Todo listo (Listeners+Comandos: "+(System.currentTimeMillis()-inicio_listeners)+"ms. / Total: "+(System.currentTimeMillis()-inicio)+"ms.)");
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    static private JDA buildJDA(){
        JDA _jda;
        DiscordBot.log("Leyendo archivo token.discordbot");
        File tokenFile=new File("token.discordbot");
        if (!tokenFile.exists() || !tokenFile.isFile()){
            DiscordBot.log("El archivo de token de Discord \"token.discordbot\" no existe. Generando uno.");
            try {
                tokenFile.createNewFile();
            } catch (IOException ex) {
                DiscordBot.log("Error creando el archivo: "+ex.getMessage());
                ex.printStackTrace();
            }
            return null;
        }
        
        if(!tokenFile.canRead()){
            DiscordBot.log("No se puede leer el archivo \"token.discordbot\".");
            return null;
        }
        
        if(tokenFile.length() == 0){
            System.err.println("El archivo token \"token.discordbot\" no es válido.");
            return null;
        }
        
        String token=loadTokenFromFile(tokenFile);
        DiscordBot.log("Token cargado.");
        try {
            DiscordBot.log("Conectando a Discord...");
            _jda=JDABuilder.create(token, GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_VOICE_STATES, GatewayIntent.GUILD_MESSAGES, GatewayIntent.GUILD_MESSAGE_REACTIONS, GatewayIntent.DIRECT_MESSAGES, GatewayIntent.GUILD_EMOJIS_AND_STICKERS).build();
            DiscordBot.log("Orden de conexión enviada.");
            _jda.awaitReady();
            DiscordBot.log("Conexión establecida.");
        } catch (InterruptedException ex) {
            ex.printStackTrace();
            DiscordBot.log("Error al conectar a los servidores de Discord.");
            return null;
        }
        
        return _jda;
    }
    
    static private String loadTokenFromFile(File file){
        try (BufferedReader is = new BufferedReader(new FileReader(file))) {    
            return is.readLine();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            return null;
        } catch (IOException ex){
            ex.printStackTrace();
            return null;
        }
    }

    static private void registerCommands() {
        CommandListUpdateAction commands=jda.updateCommands();
        List<Command> globalCommands=commands.addCommands(
                /* Commands.slash("sorteo", "Crea un sorteo.")
                        .addOption(OptionType.STRING, "titulo", "El título del sorteo. ¿Qué sorteamos?", true)
                        .addOption(OptionType.INTEGER, "duracion", "Duración del sorteo, en días.", true)
                        .addOption(OptionType.INTEGER, "num_ganadores", "El número de ganadores que tendrá este sorteo.", true)
                        .addOption(OptionType.STRING, "descripcion", "Descripción del sorteo: Detalles adicionales (Opcional).", false),*/
        ).complete();
    }

    static private void addListeners() {
        jda.addEventListener(new CommandListener());
    }
}
